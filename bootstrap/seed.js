const emoji = require('../app/shared/constants').line.emoji
const loki = require('lokijs')
const db = new loki('pajak-bot.db', {
  autosave: true,
  autosaveInterval: 0
})

const models = db.addCollection('models')
const attributes = db.addCollection('attributes')
const users = db.addCollection('users')

models.insert([{
    intent: 'sapaan',
    replies: [],
    defaultReply: {
      type: 'text',
      text: `Hai, ada yang bisa aku bantu? ${emoji.grin}`,
      attributes: []
    }
  },
  {
    intent: 'terima-kasih',
    replies: [],
    defaultReply: {
      type: 'text',
      text: `Sama-sama ${emoji.wink} aku harap jawaban yang aku berikan dapat membantu.`,
      attributes: []
    }
  },
  {
    intent: 'simpan-profil',
    replies: [],
    defaultReply: {
      type: 'text',
      text: `Profil berhasil disimpan ${emoji.smile} kamu dapat menghapusnya kapan pun.`,
      attributes: []
    }
  },
  {
    intent: 'hapus-profil',
    replies: [],
    defaultReply: {
      type: 'text',
      text: `Profil berhasil dihapus. ${emoji.expressionless}`,
      attributes: []
    }
  },
  {
    intent: 'kewajiban-mempunyai-npwp',
    replies: [{
      type: 'text',
      text: 'Pendaftaran NPWP perlu dilakukan paling lambat 1 bulan setelah pekerjaan bebas mulai dilakukan.',
      attributes: [{
        key: 'workType',
        value: 'freelance'
      }]
    }, {
      type: 'text',
      text: 'Pendaftaran NPWP perlu dilakukan paling lambat 1 bulan setelah kamu mulai bekerja.',
      attributes: [{
        key: 'workType',
        value: 'non_freelance'
      }]
    }],
    defaultReply: {
      type: 'text',
      text: 'Kamu wajib mendaftarkan diri untuk memperoleh Nomor Pokok Wajib Pajak (NPWP) paling lambat 1 (satu) bulan setelah saat usaha, atau pekerjaan bebas nyata-nyata mulai dilakukan.',
      attributes: []
    }
  },
  {
    intent: 'npwp-bagi-orang-kawin',
    replies: [{
      type: 'text',
      text: 'Kamu tidak perlu mendaftarkan diri untuk memperoleh NPWP. Kamu harus melaksanakan hak serta memenuhi kewajiban perpajakan kamu menggunakan NPWP kepala keluarga.',
      attributes: [{
        key: 'maritalStatus',
        value: 'married'
      }, {
        key: 'taxStatus',
        value: 'unregistered'
      }, {
        key: 'maritalTaxPreference',
        value: 'combined'
      }]
    }, {
      type: 'text',
      text: 'Kamu dapat mengajukan permohonan penghapusan NPWP.',
      attributes: [{
        key: 'maritalStatus',
        value: 'married'
      }, {
        key: 'taxStatus',
        value: 'registered'
      }, {
        key: 'maritalTaxPreference',
        value: 'combined'
      }]
    }],
    defaultReply: {
      type: 'text',
      text: 'Kewajiban mendaftarkan diri untuk memperoleh Nomor Pokok Wajib Pajak (NPWP) juga berlaku bagi kamu, orang kawin, yang dikenai pajak secara terpisah karena:\n' +
        '1. hidup terpisah berdasarkan keputusan hakim;\n\n' +
        '2. menghendaki secara tertulis berdasarkan perjanjian pemisahan penghasilan dan harta; atau\n\n' +
        '3. memilih melaksanakan hak dan kewajiban perpajakan terpisah dari pasangannya meskipun tidak terdapat keputusan hakim atau tidak terdapat perjanjian pemisahan penghasilan dan harta.',
      attributes: []
    }
  },
  {
    intent: 'syarat-pendaftaran-npwp',
    replies: [{
      type: 'text',
      text: 'Syarat-syarat pendaftaran NPWP untuk WNI dengan pekerjaan bebas adalah:\n' +
        '* fotokopi Kartu Tanda Penduduk (KTP), dan\n\n' +
        '* fotokopi dokumen izin kegiatan usaha yang diterbitkan oleh instansi yang berwenang, atau\n\n' +
        '* surat keterangan tempat kegiatan usaha atau pekerjaan bebas dari Pejabat Pemerintah Daerah sekurang-kurangnya Lurah atau Kepala Desa, atau\n\n' +
        '* lembar tagihan listrik dari Perusahaan Listrik/ bukti pembayaran listrik;\n\n' +
        'atau\n\n' +
        '* fotokopi e-KTP, dan\n\n' +
        '* surat pernyataan di atas meterai yang menyatakan bahwa kamu benar-benar menjalankan usaha atau pekerjaan bebas.',
      attributes: [{
        key: 'nationality',
        value: 'indonesian'
      }, {
        key: 'workType',
        value: 'freelance'
      }]
    }, {
      type: 'text',
      text: 'Syarat-syarat pendaftaran NPWP untuk warga negara asing dengan pekerjaan bebas adalah:\n' +
        '* fotokopi paspor, fotokopi Kartu Izin Tinggal Terbatas (KITAS) atau Kartu Izin Tinggal Tetap (KITAP), dan\n\n' +
        '* fotokopi dokumen izin kegiatan usaha yang diterbitkan oleh instansi yang berwenang, atau\n\n' +
        '* surat keterangan tempat kegiatan usaha atau pekerjaan bebas dari Pejabat Pemerintah Daerah sekurang-kurangnya Lurah atau Kepala Desa, atau\n\n' +
        '* lembar tagihan listrik dari Perusahaan Listrik/ bukti pembayaran listrik.',
      attributes: [{
        key: 'nationality',
        value: 'non_indonesian'
      }, {
        key: 'workType',
        value: 'freelance'
      }]
    }, {
      type: 'text',
      text: 'Syarat-syarat pendaftaran NPWP untuk yang telah menikah adalah:\n' +
        '* fotokopi Kartu NPWP pasangan;\n\n' +
        '* fotokopi Kartu Keluarga; dan\n\n' +
        '* fotokopi surat perjanjian pemisahan penghasilan dan harta, atau surat pernyataan menghendaki melaksanakan hak dan memenuhi kewajiban perpajakan terpisah dari hak dan kewajiban perpajakan pasangan kamu.',
      attributes: [{
        key: 'maritalStatus',
        value: 'married'
      }, {
        key: 'maritalTaxPreference',
        value: 'separate'
      }]
    }],
    defaultReply: {
      type: 'uri',
      label: 'Buka',
      uri: 'http://www.pajak.go.id/content/1113-syarat-pendaftaran-npwp',
      text: 'Jawaban terlalu panjang untuk ditulis di sini. Akses jawaban di luar percakapan?',
      attributes: []
    }
  },
  {
    intent: 'pengajuan-permohonan-npwp',
    replies: [],
    defaultReply: {
      type: 'uri',
      label: 'Buka',
      uri: 'http://www.pajak.go.id',
      text: 'Kamu dapat mengajukan permohonan pendaftaran Nomor Pokok Wajib Pajak (NPWP) secara elektronik melalui aplikasi e-Registration yang tersedia pada laman Direktorat Jenderal Pajak di www.pajak.go.id ataupun secara tertulis.',
      attributes: []
    }
  },
  {
    intent: 'perubahan-data-wajib-pajak',
    replies: [],
    defaultReply: {
      type: 'text',
      text: 'Dalam hal terdapat perubahan data, yaitu data yang terdapat dalam administrasi perpajakan berbeda dengan data kamu menurut keadaan yang sebenarnya yang tidak memerlukan pemberian Nomor Pokok Wajib Pajak (NPWP) baru, kamu wajib melaporkan perubahan tersebut ke Kantor Pelayanan Pajak (KPP) yang wilayah kerjanya meliputi tempat tinggal dan/atau tempat kegiatan usaha kamu.\n\n' +
        'Termasuk dalam perubahan data tersebut berupa:\n' +
        '1. perubahan identitas;\n\n' +
        '2. perubahan alamat tempat tinggal yang masih dalam wilayah kerja KPP yang sama;\n\n' +
        '3. perubahan kategori;\n\n' +
        '4. perubahan sumber penghasilan utama.\n\n' +
        'Selain atas permohonan kamu, perubahan data juga dapat dilakukan secara jabatan oleh KPP.',
      attributes: []
    }
  },
  {
    intent: 'penghapusan-npwp',
    replies: [],
    defaultReply: {
      type: 'text',
      text: 'Apabila persyaratan subjektif dan/atau objektif sesuai dengan ketentuan peraturan perundang-undangan di bidang perpajakan sudah tidak terpenuhi lagi, maka penghapusan Nomor Pokok Wajib Pajak (NPWP) dapat dilakukan, yaitu dalam hal:\n' +
        '1. Kamu telah meninggal dunia dan tidak meninggalkan warisan;\n\n' +
        '2. Kamu telah meninggalkan Indonesia untuk selama-lamanya;\n\n' +
        '3. Kamu memiliki lebih dari 1 (satu) NPWP untuk menentukan NPWP yang dapat digunakan sebagai sarana administratif dalam pelaksanaan hak dan pemenuhan kewajiban perpajakan;\n\n' +
        '4. Kamu berstatus sebagai pengurus, komisaris, pemegang saham/pemilik dan pegawai yang telah diberikan NPWP melalui pemberi kerja/bendahara pemerintah dan penghasilan netonya tidak melebihi Penghasilan Tidak Kena Pajak (PTKP);\n\n' +
        '5. Warisan yang belum terbagi dalam kedudukan sebagai Subjek Pajak sudah selesai dibagi;\n\n' +
        '6. Wanita yang sebelumnya telah memiliki NPWP dan menikah tanpa membuat perjanjian pemisahan harta dan penghasilan serta tidak ingin melaksanakan hak dan memenuhi kewajiban perpajakannya terpisah dari suaminya;\n\n' +
        '7. Wanita kawin yang memiliki NPWP berbeda dengan NPWP suami dan pelaksanaan hak dan pemenuhan kewajiban perpajakannya digabungkan dengan pelaksanaan hak dan pemenuhan kewajiban perpajakan suami; atau\n\n' +
        '8. Anak belum dewasa yang telah memiliki NPWP.',
      attributes: []
    }
  },
  {
    intent: '*',
    defaultReply: {
      type: 'text',
      text: `Maaf, aku tidak mengerti maksud kamu. ${emoji.embarrassedWithSweat}`,
      attributes: []
    }
  }
])

attributes.insert([{
  key: 'workType',
  values: [
    { id: 'freelance', text: 'Bebas' },
    { id: 'permanent', text: 'Permanen' },
    { id: 'contract', text: 'Kontrak' }
  ],
  questions: ['Apa jenis pekerjaan kamu?']
}, {
  key: 'workStatus',
  values: [
    { id: 'started', text: 'Ya' },
    { id: 'not_started', text: 'Tidak' },
    { id: 'stopped', text: 'Telah Berhenti' }
  ],
  questions: ['Apakah kamu telah mulai bekerja?']
}, {
  key: 'nationality',
  values: [
    { id: 'indonesian', text: 'Ya' },
    { id: 'non_indonesian', text: 'Bukan' }
  ],
  questions: ['Apakah kamu seorang warga negara Indonesia?']
}, {
  key: 'taxStatus',
  values: [
    { id: 'registered', text: 'Ya' },
    { id: 'unregistered', text: 'Tidak' }
  ],
  questions: ['Apakah kamu telah mempunyai Nomor Pokok Wajib Pajak (NPWP)?']
}, {
  key: 'maritalStatus',
  values: [
    { id: 'married', text: 'Ya' },
    { id: 'single', text: 'Tidak' }
  ],
  questions: ['Apakah kamu telah menikah?']
}, {
  key: 'maritalTaxPreference',
  values: [
    { id: 'separate', text: 'Ya' },
    { id: 'combined', text: 'Tidak' }
  ],
  questions: ['Apakah kamu ingin melaksanakan hak dan kewajiban perpajakan secara terpisah dari pasangan?']
}])

// const exampleUser = {
//   id: 'user id',
//   attributeAsked: '',
//   ...attributes
// }

db.close()