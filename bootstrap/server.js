const restify = require('restify')
const morgan = require('morgan')

const server = restify.createServer({
  name: 'pajak-bot',
  version: '0.0.1'
})

server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.bodyParser({ mapParams: true }))
server.use(morgan('dev'))

// CORS
server.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  next()
})

server.opts(/\.*/, function(req, res, next) {
  res.send(200)
  next()
})

// Setup default document.
const defaultDoc = 'index.html'
server.use((req, res, next) => {
  req.url = (req.url === '/') ? '/' + defaultDoc : req.url
  return next()
})

module.exports = server