const loki = require('lokijs')

var models, attributes, users

const db = new loki('pajak-bot.db', {
  autoload: true,
  autoloadCallback: initDb,
  autosave: true,
  autosaveInterval: 0
})

const exported = {}

function initDb() {
  models = db.getCollection('models')
  attributes = db.getCollection('attributes')
  users = db.getCollection('users')

  if (!(models && attributes && users)) {
    throw Error('database not properly seeded')
  }

  exported['models'] = models
  exported['attributes'] = attributes
  exported['users'] = users
}

module.exports = exported