const server = require('../../bootstrap/server')
const util = require('../shared/util')
const line = require('../shared/line')
const wit = require('../shared/wit')
const lineBot = require('../shared/line-bot')

line.listenTo('message', lineBot.handleMessage)
line.listenTo('postback', lineBot.handlePostback)

server.post('/line/webhook', (req, res, next) => {
  if (!line.verifySig(req)) {
    util.renderRes(res, 401, {})
    return next()
  }

  if (req.params.events) {
    console.log(req.params.events)
    req.params.events.forEach(event => line.call(event))
  }

  util.renderRes(res, 200, {})
  return next()
})

server.post('/wit/test', (req, res, next) => {
  wit.message(req.params.message)
    .then(data => {
      console.log(wit.highestConfidenceIntent(data))
    })
    .catch(err => console.error(err))

  util.renderRes(res, 200, {})
  return next()
})