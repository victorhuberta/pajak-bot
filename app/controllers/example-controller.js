const server = require('../../bootstrap/server')
const util = require('../shared/util')
const db = require('../../bootstrap/database')
const mongojs = require('mongojs')

const loopValidation = {
  content: {
    question: { isRequired: true, max: 140 },
    background: { isRequired: true },
    type: { isRequired: true, isAlpha: true },
    options: { isRequired: true }
  }
}

function constructLoop(req) {
  return {
    question: req.params.question,
    background: req.params.background,
    type: req.params.type,
    options: req.params.options
  }
}

server.get('/loops', (req, res, next) => {
  db.loops.find((err, data) => {
    util.handleErr(res, err, () => {
      util.renderRes(res, 200, data)
    })
  })

  return next()
})

server.get('/loops/:id', (req, res, next) => {
  db.loops.findOne({
    _id: mongojs.ObjectId(req.params.id)
  }, (err, data) => {
    util.handleErr(res, err, () => {
      util.renderRes(res, 200, data)
    })
  })

  return next()
})

server.post({
  url: '/loops',
  validation: loopValidation
}, (req, res, next) => {
  db.loops.insert(constructLoop(req), (err, data) => {
    util.handleErr(res, err, () => {
      util.renderRes(res, 200, data)
    })
  })

  return next()
})

server.put({
  url: '/loops/:id',
  validation: Object.assign({
    resources: {
      id: { isRequired: true }
    }
  }, loopValidation)
}, (req, res, next) => {
  db.loops.update({
    _id: mongojs.ObjectId(req.params.id)
  }, constructLoop(req), (err, data) => {
    util.handleErr(res, err, () => {
      util.renderRes(res, 200, data)
    })
  })

  return next()
})

server.del('/loops/:id', (req, res, next) => {
  db.loops.update({
    _id: mongojs.ObjectId(req.params.id)
  }, {
    isDeleted: true
  }, (err, data) => {
    util.handleErr(res, err, () => {
      util.renderRes(res, 200, data)
    })
  })

  return next()
})

server.get('/loopTypes', (req, res, next) => {
  db.loopTypes.find((err, data) => {
    util.handleErr(res, err, () => {
      util.renderRes(res, 200, data)
    })
  })

  return next()
})