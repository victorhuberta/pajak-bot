const wit = require('./wit')
const line = require('./line')
const util = require('./util')
const db = require('../../bootstrap/database')

const botActions = {
  'hapus-profil': (event) => {
    const user = { id: event.source.userId }
    db.users.findAndRemove({ id: user.id })
    db.users.insert(user)
  }
}

function handleMessage(event) {
  if (event.message && event.message.type === 'text') {
    // Get meaning of sentence.
    wit.message(event.message.text)
      .then(data => handleWitData(event, data))
      .catch(err => console.error(err))
  }
}

function handleWitData(event, witData) {
  const intent = wit.highestConfidenceIntent(witData)
  const model = intent ? db.models.findOne({ intent }) : null

  // Perform bot action before replying.
  const callback = botActions[intent]
  if (model && callback) {
    callback(event)
  }
  reply(event, witData)
}

function handlePostback(event) {
  if (!(event.source && event.source.type === 'user' && event.source.userId)) return

  const user = findUser(event)

  if (event.postback && event.postback.data) {
    const data = JSON.parse(event.postback.data)
    if (data && data.id && user.attributeAsked) {
      // Save asked attribute's value.
      user[user.attributeAsked] = data.id
      user.attributeAsked = null
      db.users.update(user)

      if (user.unresolved && user.unresolved.event && user.unresolved.witData) {
        // Reply to unresolved event.
        const newEvent = user.unresolved.event
        newEvent.replyToken = event.replyToken
        reply(newEvent, user.unresolved.witData)
      }
    }
  }
}

function reply(event, witData) {
  if (!(event.source && event.source.type === 'user' && event.source.userId)) return

  const user = findUser(event)
  const intent = wit.highestConfidenceIntent(witData)
  const model = intent ? db.models.findOne({ intent }) : null
  if (model) {
    const selectedReply = _selectReply(user, model)
    const thingsToAsk = _findThingsToAsk(user, selectedReply)
    console.log('selected reply', selectedReply)
    console.log('things to ask', thingsToAsk)

    if (thingsToAsk.length > 0) {
      // Save unresolved event and ask user for information.
      user['attributeAsked'] = thingsToAsk[0]
      user['unresolved'] = user['unresolved'] ? user['unresolved'] : { event, witData }
      ask(event.replyToken, user['attributeAsked'])
    } else {
      // Resolve event and send message to user.
      let selectedReplies = [selectedReply]
      if (user['unresolved']) {
        const saveProfileReply = getReplyByIntent('simpan-profil')
        if (saveProfileReply) {
          selectedReplies.unshift(saveProfileReply)
        }
      }
      message(event.replyToken, selectedReplies)
      user['unresolved'] = null
    }
    db.users.update(user)
  } else {
    replyDefault(event)
  }
}

function findUser(event) {
  let user = db.users.findOne({ id: event.source.userId })
  if (!user) {
    user = { id: event.source.userId }
    db.users.insert(user)
  }
  return user
}

function _selectReply(user, model) {
  // Select one reply probabilistically based on relevance measure.
  let bucket = [model.defaultReply]
  model.replies.forEach(reply => {
    let points = 0
    let totallyNotInProfile = true

    for (attribute of reply.attributes) {
      if (!user[attribute.key]) continue
      else totallyNotInProfile = false

      if (attribute.value === user[attribute.key]) {
        // +2 if attribute's value matches user's profile.
        points += 2
      } else {
        // Do not include a wrong reply in the bucket.
        points = 0
        break
      }
    }
    if (totallyNotInProfile) {
      // +1 if user's profile does not contain attributes.
      points = 1
    }
    bucket = bucket.concat(Array(points).fill(reply))
  })
  console.log('reply bucket', bucket)
  return util.randomFrom(bucket)
}

function _findThingsToAsk(user, selectedReply) {
  // Find out what to ask the user.
  let bucket = []
  selectedReply.attributes.forEach(attribute => {
    if (!user[attribute.key]) {
      bucket.push(attribute.key)
    }
  })
  return bucket
}

// Ask the user for an attribute's value.
function ask(replyToken, attributeKey) {
  const attribute = db.attributes.findOne({ key: attributeKey })
  if (!(attribute && attribute.values && attribute.questions)) return

  const selectedQuestion = util.randomFrom(attribute.questions)
  const actions = attribute.values.map(value => {
    return {
      label: value.text,
      data: JSON.stringify(value)
    }
  })
  line.replyPostback(replyToken, selectedQuestion, actions)
}

function getReplyByIntent(intent) {
  const model = db.models.findOne({ intent })
  return model ? model.defaultReply : null
}

// Reply with fallback message.
function replyDefault(event) {
  const model = db.models.findOne({ intent: '*' })
  if (!(model && model.defaultReply)) return

  message(event.replyToken, [model.defaultReply])
}

function message(replyToken, replies) {
  const messages = replies.map(reply => {
    switch (reply.type) {
      case 'text':
        return line.createTextMessage(reply.text)
      case 'uri':
        return line.createUriMessage(reply.text, [{ label: reply.label, uri: reply.uri }])
    }
  })

  line.replyMessage(replyToken, messages)
    .then(res => console.log(res))
    .catch(err => {
      if (err instanceof line.sdk.HTTPError) {
        console.error(err.statusCode + ' - ' + err.statusMessage + ': ' + JSON.stringify(err.originalError.response.data))
      }
    })
}

module.exports = {
  handleMessage,
  handlePostback
}