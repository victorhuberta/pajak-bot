const { Wit, log } = require('node-wit')
const WIT = require('./constants').wit

const client = new Wit({
  accessToken: WIT.accessToken,
  logger: new log.Logger(log.DEBUG)
})

function message(msg, context = {}) {
  return client.message(msg, context)
}

function highestConfidenceIntent(data) {
  if (!data.entities || !data.entities.intent) return null
  const intents = data.entities.intent

  intents.sort((a, b) => b.confidence - a.confidence)
  return intents[0].value
}

module.exports = {
  client,
  message,
  highestConfidenceIntent
}