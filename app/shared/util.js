const Errors = {
  unameExists: 333,
  duplicateKey: 11000
}

class Err {
  constructor(code, message) {
    this.code = code
    this.message = message
  }

  toJSON() {
    return {
      code: this.code,
      message: this.message
    }
  }
}

function handleErr(res, err, success) {
  if (err !== null) {
    if (err.name === 'MongoError') {
      switch (err.code) {
        default: break
      }
    }

    renderRes(res, 422, err)
  } else {
    success()
  }
}

function renderRes(res, status, data) {
  res.writeHead(status, {
    'Content-Type': 'application/json; charset=utf-8'
  })
  res.end(JSON.stringify(data))
}

function randomFrom(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

module.exports = {
  handleErr,
  renderRes,
  randomFrom
}