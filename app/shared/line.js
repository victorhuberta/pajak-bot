const LINE = require('./constants').line
const sdk = require('@line/bot-sdk')
const client = new sdk.Client({
  channelAccessToken: LINE.channelAccessToken,
  channelSecret: LINE.channelSecret
})

function verifySig(req) {
  // return true
  if (!req || !req.rawBody) return false;

  return sdk.validateSignature(req.rawBody, LINE.channelSecret, req.header('X-Line-Signature'))
}

const eventListeners = {}

function listenTo(eventName, callback) {
  eventListeners[eventName] = callback
}

function call(event) {
  if (!event.type) return
  const callback = eventListeners[event.type]
  return callback ? callback(event) : undefined
}

function replyMessage(replyToken, messages) {
  console.log('message', replyToken, JSON.stringify(messages))
    // return Promise.resolve()

  return client.replyMessage(replyToken, messages)
}

function replyPostback(replyToken, message, actions) {
  console.log('postback', replyToken, message, JSON.stringify(actions))
    // return Promise.resolve()

  return client.replyMessage(replyToken, {
    type: 'template',
    altText: message,
    template: {
      type: 'buttons',
      text: message,
      actions: actions.map(action => {
        return {
          type: 'postback',
          label: action.label,
          data: action.data
        }
      })
    }
  })
}

function createTextMessage(message) {
  return {
    type: 'text',
    text: message
  }
}

function createPostbackMessage(message, actions) {
  return {
    type: 'template',
    altText: message,
    template: {
      type: 'buttons',
      text: message,
      actions: actions.map(action => {
        return {
          type: 'postback',
          label: action.label,
          data: action.data
        }
      })
    }
  }
}

function createUriMessage(message, actions) {
  return {
    type: 'template',
    altText: message,
    template: {
      type: 'buttons',
      text: message,
      actions: actions.map(action => {
        return {
          type: 'uri',
          label: action.label,
          uri: action.uri
        }
      })
    }
  }
}

module.exports = {
  sdk,
  client,
  verifySig,
  listenTo,
  call,
  replyMessage,
  replyPostback,
  createTextMessage,
  createPostbackMessage,
  createUriMessage
}