const restify = require('restify')
const server = require('./bootstrap/server')

// Load all controllers.
require('./app/controllers/chat-controller')

// Setup public folder.
const path = require('path')
server.get(/\/(.*)?.*/, restify.plugins.serveStatic({
  directory: path.join(__dirname, 'public')
}))

const port = process.env.PORT || 8080
server.listen(port, () => {
  console.log('%s listening at %s', server.name, server.url)
})